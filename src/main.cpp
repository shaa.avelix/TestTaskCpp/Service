#include <QCoreApplication>
#include <QCommandLineParser>

#include <QDateTime>
#include <QSettings>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>

#include "Service.h"

enum ApplicationError
{
  ParseCommandOptionsFailure = -0x01,

  ServiceStartFailure = -0x10,
  SetDatabaseFailure = -0x11
};

static void open_logfile(QString name);
static void qt_message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
static void load_settings(Service & srv);
static bool parse_cmd_ops(QCoreApplication & app, Service & srv);
int main(int argc, char * argv[])
{
  open_logfile("service.log");
  qInstallMessageHandler(qt_message_handler);
  QCoreApplication a(argc, argv);
  Service srv;

  qInfo().noquote().nospace() << QDateTime::currentDateTime().toString("\n\n==={ yyyy-MM-dd hh:mm:ss.zzz }===\n");

  load_settings(srv);
  if (!parse_cmd_ops(a, srv)) return ParseCommandOptionsFailure;
  if (!srv.start()) return ServiceStartFailure;

  return a.exec();
}

static void load_settings(QSettings & settings, Service & srv)
{
  if (!QFileInfo::exists(settings.fileName())) return;

  settings.beginGroup("Main");
  if (settings.contains("port"))
    srv.setPort(settings.value("port").toInt());
  settings.endGroup();
  settings.beginGroup("PersonalData");
  if (settings.contains("driver"))
    srv.setDatabaseDriver(settings.value("driver").toString());
  if (settings.contains("host"))
    srv.setDatabaseHost(settings.value("host").toString());
  if (settings.contains("user"))
    srv.setDatabaseUser(settings.value("user").toString());
  if (settings.contains("password"))
    srv.setDatabasePassword(settings.value("password").toString());
  if (settings.contains("name"))
    srv.setDatabaseName(settings.value("name").toString());
  settings.endGroup();
}
static void load_settings(Service & srv)
{
  QSettings userScopeSettings(QSettings::IniFormat, QSettings::UserScope, "Avelix", "TestTaskCpp-Service");
  QSettings currentDirSettings("service.ini", QSettings::IniFormat);
  load_settings(userScopeSettings, srv);
  load_settings(currentDirSettings, srv);
}

static bool parse_cmd_ops(QCoreApplication & app, Service & srv)
{
  QCommandLineParser parser;
  parser.setApplicationDescription("Avelix Test Task Service");
  parser.addHelpOption();

  QCommandLineOption portOption(QStringList() << "p" << "port",
          QCoreApplication::translate("main", "Set port for listen"), "port", "7777");
  parser.addOption(portOption);

  QCommandLineOption sqlDrvOption(QStringList() << "d" << "sql-driver",
          QCoreApplication::translate("main", "Set Sql Driver"), "drv", "QMYSQL");
  parser.addOption(sqlDrvOption);

  QCommandLineOption dbHostOption(QStringList() << "s" << "db-host",
          QCoreApplication::translate("main", "Set database host"), "host");
  parser.addOption(dbHostOption);

  QCommandLineOption dbNameOption(QStringList() << "b" << "db-name",
          QCoreApplication::translate("main", "Set database name"), "name");
  parser.addOption(dbNameOption);

  QCommandLineOption dbUsrOption(QStringList() << "u" << "db-usr",
          QCoreApplication::translate("main", "Set database user"), "usr");
  parser.addOption(dbUsrOption);

  QCommandLineOption dbPwdOption(QStringList() << "p" << "db-pwd",
          QCoreApplication::translate("main", "Set database user password"), "pwd");
  parser.addOption(dbPwdOption);

  QCommandLineOption sqlDrvListOption(QStringList() << "sql-drivers-list",
          QCoreApplication::translate("main", "Show avaliable Sql Drivers list"));
  parser.addOption(sqlDrvListOption);

  parser.process(app);

  if (parser.isSet(sqlDrvListOption))
  {
    qInfo() << "Sql Drivers:\n" << QSqlDatabase::drivers();
    ::exit(EXIT_SUCCESS);
  }

  if (parser.isSet(portOption))
    srv.setPort(parser.value(portOption).toInt());

  if (parser.isSet(sqlDrvOption))
    srv.setDatabaseDriver(parser.value(sqlDrvOption));
  if (parser.isSet(dbHostOption))
    srv.setDatabaseHost(parser.value(dbHostOption));
  if (parser.isSet(dbNameOption))
    srv.setDatabaseName(parser.value(dbNameOption));
  if (parser.isSet(dbUsrOption))
    srv.setDatabaseUser(parser.value(dbUsrOption));
  if (parser.isSet(dbPwdOption))
    srv.setDatabasePassword(parser.value(dbPwdOption));
  return true;
}

static QFile logFile;
static void open_logfile(QString name)
{
  logFile.setFileName(name);
  bool res = logFile.open(QFile::Append | QFile::Text);
  if (!res)
    qWarning() << "Open log-file failure:" << logFile.errorString();
}

static void qt_message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
  QTextStream out(&logFile);
  QByteArray localMsg = msg.toLocal8Bit();
  switch (type) {
  case QtDebugMsg:
    fprintf(stderr, "[Debug] %s\n", localMsg.constData());
    break;
  case QtInfoMsg:
    fprintf(stderr, "%s\n", localMsg.constData());
    out << localMsg.constData() << endl;
    break;
  case QtWarningMsg:
    fprintf(stderr, "[Warning] %s\n", localMsg.constData());
    break;
  case QtCriticalMsg:
    fprintf(stderr, "!Critical! %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
    break;
  case QtFatalMsg:
    fprintf(stderr, "!!Fatal!! %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
    break;
  }
}
