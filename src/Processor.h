#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>

#include <QTcpSocket>
#include <QSqlDatabase>
#include <QGeoServiceProvider>

#include <QJsonObject>
#include <QByteArray>

class Processor : public QObject
{
  Q_OBJECT
public:
  explicit Processor(QTcpSocket * client, QSqlDatabase * db, QGeoServiceProvider * geoServiceProvider, QObject *parent = nullptr);

signals:

public slots:

protected:
  QTcpSocket * tcpClient;
  QSqlDatabase * db;
  QGeoServiceProvider * geoServiceProvider;

  QByteArray packetBuf;

  void packet_handler(QByteArray & packet);

  void packet_handler(QJsonObject json);
  void read_persons(QJsonArray json);
  void read_person(QJsonObject json);

  void person_handler(QString & firstname, QString & lastname, QString & address);

  QString peerStr;

protected slots:
  void read_data();
  void close();
  void same_places_handler(bool isSame, QVariant userData);
};

#endif // PROCESSOR_H
