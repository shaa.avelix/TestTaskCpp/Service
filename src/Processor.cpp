#include "Processor.h"

#include <QHostAddress>

#include <QJsonDocument>
#include <QJsonArray>

#include <QSqlQuery>

#include "AddressService.h"

Processor::Processor(QTcpSocket* client, QSqlDatabase* db, QGeoServiceProvider * geoServiceProvider, QObject *parent) :
  QObject(parent)
{
  Q_ASSERT(client); 
  tcpClient = client;

  peerStr = tcpClient->peerAddress().toString() + ":" + QString::number(tcpClient->peerPort());

  connect(tcpClient, &QTcpSocket::readyRead, this, &Processor::read_data);
  connect(tcpClient, &QAbstractSocket::disconnected, this, &Processor::close);

  Q_ASSERT(db->isOpen());
  this->db = db;

  Q_ASSERT(geoServiceProvider);
  this->geoServiceProvider = geoServiceProvider;

  qInfo().noquote() << " -> new connection" << peerStr;
}

void Processor::close()
{
  qInfo().noquote() << " -x disconnect" << peerStr;

  disconnect(tcpClient, &QTcpSocket::readyRead, this, &Processor::read_data);
  tcpClient->deleteLater();
}

void Processor::person_handler(QString& firstname, QString& lastname, QString& address)
{
  QString fullname = firstname+" "+lastname;

  AddressService * addrSrv = new AddressService(geoServiceProvider,parent());
  connect(addrSrv, &AddressService::same_places, this, &Processor::same_places_handler);
  connect(addrSrv, &AddressService::ready, addrSrv, &AddressService::deleteLater);

  addrSrv->setBaseAddress(address, fullname);

  QString queryStr = QString(
        "SELECT address FROM `PersonalData` "
        "WHERE `firstname` LIKE '%1' AND `lastname` LIKE '%2'")
      .arg(firstname).arg(lastname);
  QSqlQuery query(queryStr,*db);
  while(query.next())
  {
    QString address0 = query.value(0).toString();
    qInfo() << "   Packet from" << peerStr << "contains information about" << fullname;

    addrSrv->checkAddress(address0);
    break; // TODO: add suppurt for multiple persons
  }
}

void Processor::same_places_handler(bool isSame, QVariant userData)
{
  if (isSame)
    qInfo() << "   Packet from" << peerStr << "contains address of" << userData.toString();
}

void Processor::read_data()
{
  QByteArray rxBuf = tcpClient->readAll();
  for (int i = 0; i < rxBuf.size(); ++i)
  {
    if ((rxBuf[i] == '\n')&&(packetBuf.size()))
    {
      packet_handler(packetBuf);
      packetBuf.clear();
    }
    else
      packetBuf.append(rxBuf[i]);
  }
}

void Processor::packet_handler(QByteArray& packet)
{
  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(packet, &err);
  if (err.error)
  {
    qWarning() << "Processor: json parse failure: " << err.errorString()
               << "\npacket:\n" << QString::fromUtf8(packet);
    return;
  }
  packet_handler(doc.object());
}

#define JSON_READ(json,name,type,retval) \
do{ \
  if (json.contains(name) && json[name].is##type()) \
    retval = json[name].to##type(); \
}while(0)

#define JSON_READ_AND_HANDLE(json,name,type,handler) \
do{ \
  if (json.contains(name) && json[name].is##type()) \
    handler(json[name].to##type()); \
}while(0)

void Processor::packet_handler(QJsonObject json)
{
  JSON_READ_AND_HANDLE(json,"persons",Array,read_persons);
  JSON_READ_AND_HANDLE(json,"person",Object,read_person);
}

void Processor::read_persons(QJsonArray json)
{
  for (int i = 0; i < json.count(); ++i)
    read_person(json.at(i).toObject());
}

void Processor::read_person(QJsonObject json)
{
  QString Firstname,Lastname,Address;
  JSON_READ(json,"firstname",String,Firstname);
  JSON_READ(json,"lastname",String,Lastname);
  JSON_READ(json,"address",String,Address);
  person_handler(Firstname,Lastname,Address);
}
