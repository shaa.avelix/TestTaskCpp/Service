#include "Service.h"
#include "Processor.h"

#include <QSqlError>

#include <QDebug>

Service::Service(QObject *parent) :
  QObject(parent),
  tcpServer(this)
{
  geoServiceProvider = nullptr;
  geoServiceProviderName = "osm";
}

void Service::setDatabaseDriver(QString sqlDriver){dbDriver = sqlDriver;}
void Service::setDatabaseHost(QString host){dbHost = host;}
void Service::setDatabaseName(QString name){dbName = name;}
void Service::setDatabaseUser(QString usr){dbUser = usr;}
void Service::setDatabasePassword(QString pwd){dbPwd = pwd;}
bool Service::openDatabase()
{
  qInfo() << "Openning database...";
  db = QSqlDatabase::addDatabase(dbDriver);
  db.setHostName(dbHost);
  db.setDatabaseName(dbName);
  db.setUserName(dbUser);
  db.setPassword(dbPwd);
  if (!db.open())
  {
    qWarning() << "Service: open database failure:" << db.lastError().databaseText();
    return false;
  }
  qInfo() << "Database openned:\n * host: " << dbHost << "\n * name: " << dbName;
  return true;
}

void Service::setGeoServiceProvider(QString provider){geoServiceProviderName = provider;}

bool Service::startGeoService()
{
  qInfo() << "Starting GeoService...";
  if (geoServiceProvider) delete geoServiceProvider;
  geoServiceProvider = new QGeoServiceProvider(geoServiceProviderName);
  if (geoServiceProvider->error() != QGeoServiceProvider::NoError)
  {
    qWarning() << "Service: connect to GeoServiceProvider failure: " << geoServiceProvider->errorString();
    return false;
  }
  qInfo() << "GeoService started";
  return true;
}

bool Service::start()
{
  if (!db.isOpen())
    if (!openDatabase())
      return false;

  if (!geoServiceProvider)
    if (!startGeoService())
      return false;

  if (!tcpServer.listen(QHostAddress::Any, tcpPort))
  {
    qWarning() << "Service: listen failure: " << tcpServer.errorString();
    return false;
  }
  connect(&tcpServer, &QTcpServer::newConnection, this, &Service::newClient);
  qInfo().nospace() << "listening TCP port " << tcpPort << "...";
  return true;
}

void Service::stop()
{
  disconnect(&tcpServer, &QTcpServer::newConnection, this, &Service::newClient);
  tcpServer.close();
}

void Service::newClient()
{
  new Processor(tcpServer.nextPendingConnection(), &db, geoServiceProvider, this);
  // will be deleted automatically when the connection is broken
}
